call plug#begin()

"Add the ability to surround text
Plug 'tpope/vim-surround'

"Add color matching to brackets
Plug 'luochen1990/rainbow'

"Add gruvbox color scheme
Plug 'morhetz/gruvbox'

"Add fuzzy finding to emulate IntelliJ's go to file
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

"Add ack to emulate IntelliJ's find in path
Plug 'mileszs/ack.vim'

"Add start menu to vim
Plug 'mhinz/vim-startify'

"Add a lightweight status line
Plug 'itchyny/lightline.vim'

"Add a buffer tabline (replaces default tabline)
Plug 'ap/vim-buftabline'

"Add asynchronous linting
Plug 'w0rp/ale'

"Add autocomplete
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'

"Add generic autocompletion
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'

"Add CSS autocompletion
Plug 'ncm2/ncm2-cssomni'

"Add JS autocompletion
Plug 'ncm2/ncm2-tern',  {'do': 'npm install'}

"Add Python autocompletion
Plug 'ncm2/ncm2-jedi'
Plug 'davidhalter/jedi-vim'

"Add automatic tagging
Plug 'ludovicchabant/vim-gutentags'

call plug#end()

"Enable color matching for brackets
let g:rainbow_active = 1

"Set colorscheme to gruvbox dark
colorscheme gruvbox
set background=dark

"Set dependencies for ncm2
autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect

"Enable moving through ncm2 suggestions with tab
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

"Set hybrid numbers
set number relativenumber

"Enable opening buffers without saving
set hidden

"Set lightline theme to gruvbox
let g:lightline = {}
let g:lightline.colorscheme = 'gruvbox'

"Set leader to space
let mapleader = " "

"Map FZF
map <leader>o :FZF <cr>

"Map ack
map <leader>f :Ack 

"Map movement between buffers
map gT :bprevious <cr>
map gt :bnext <cr>

"Map closing of buffer
map <leader>w :bd <cr>

"Map returning to start screen
map <leader>s :Startify <cr>

"Enable indentation for Python
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

"Enable indentation for front-end
au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2

"Enable truecolors
set termguicolors
